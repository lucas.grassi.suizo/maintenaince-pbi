import shutil


def copy(*args):
    args_size = len(args)
    try:
        if args_size == 1:
            copy_several_files(args)
        elif args_size > 1:
            copy_file(src=args[0], dst=args[1])
        else:
            raise Exception('The value of args not should be 0 or minor than cero, and is {}'.format(args_size))
    except Exception as not_args_error:
        print(not_args_error)



def copy_several_files(*args):
    """
    This method copy several files (more than one), into destiny paths
    :param args: a matrix of two dimensions with source and destiny paths.

    :return: None
    """
    for row in args:
        for paths in row:
            for path in paths:
                src_path = path[0]
                dst_path = path[1]
                copy_file(src_path, dst_path)


def copy_file(src, dst):
    """
    :param src: Source path in string type, with containing extension file.
    :param dst: Destiny path
    :return: None
    """
    shutil.copy(src, dst)
    print("File " + src + " was copied in " + dst + " destination succesfully.")

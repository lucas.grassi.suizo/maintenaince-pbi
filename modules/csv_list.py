import csv

def csv_to_list(path_object, delimiter):
    """

    :param path_object: Wait for a Path object that contains the path of csv file
    :param delimiter: Wait fot delimiter as str type
    :return:
    """
    try:
        csv_list = []
        with path_object.open() as f:
            csvfile = list(csv.reader(f, delimiter=delimiter))
            for row in csvfile:
                for item in row:
                    item_split = item.split(',')
                    csv_list.append(item_split)
        return csv_list
    except FileNotFoundError as fnf_error:
        print(fnf_error)        
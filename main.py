from modules import copyer, csv_list
from pathlib import Path, PurePath
import sys
import os

path_test = os.path.join(sys.argv[0], sys.argv[1])
DELIMITER = sys.argv[2]

if __name__ == '__main__':

    paths_list = csv_list.csv_to_list(Path(path_test), DELIMITER)
    copyer.copy(paths_list)
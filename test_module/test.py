from modules import csv_list, copyer
from pathlib import Path

SRC_ONE_FILE = "./test_files/test_path_1_file.csv"
SRC_SEVERAL_FILES = "./test_files/test_path_more_than_1_file.csv"
SRC_VOID_FILE = "./test_files/test_path_void_file.csv"
DELIMITER = ';'


def run_test():
    test_copy_of_one_file()
    test_copy_of_several_files()
    test_copy_of_void_files()
    pass


def test_copy_of_one_file():
    path_object = Path(__file__).parent / SRC_ONE_FILE
    paths_list = csv_list.csv_to_list(path_object, DELIMITER)
    src = paths_list[0][0]
    dst = paths_list[0][1]

    copyer.copy(src, dst)


def test_copy_of_several_files():
    path_object = Path(__file__).parent / SRC_SEVERAL_FILES
    paths_list = csv_list.csv_to_list(path_object, DELIMITER)
    copyer.copy(paths_list)


def test_copy_of_void_files():
    copyer.copy()
